from Constans import *
from random import randint
from Player import Player

players = []

discs: list = [] # A korongokat (mint listákat) tartalmazó lista
tiles: int = [20, 20, 20, 20, 20] # A zsákban lévő csempék száma
tileNames: str = ["A", "B", "C", "D", "E"] # A csempék megnevezései
desk: dict = {"A": 0, "B": 0, "C": 0, "D": 0, "E": 0, "X": 1}

def GetRandomTile(): # Ez a függvény súlyozottan kivesz egy véletlenszerű csempét a "zsákból" és visszaadja
	randTop = sum(tiles) # A tiles összege. A súlyozott véletlen kiválasztáshoz kell
	randomValue = randint(0, randTop) # 0-tól a tiles összegéig egy érték
	sumOfPrevious: int = 0 # A for ciklusban eddig vett, nem megfelelő értékek összeadva

	# TODO: Figyelnie kell, hogy kiürült-e a zsák és visszaadnia az értéket
	for i, tile in enumerate(tiles, start=0):
		if (tile + sumOfPrevious > randomValue): # Súlyozott véletlenszerű kiválasztás feltétele
			tiles[i] -= 1 # Kivonunk egyet a tiles-ból, mivel kivettünk egy csempét a zsákból
			return tileNames[i] # Visszaadjuk a kivett csempe nevét
		else:
			sumOfPrevious += tile # Hozzáadjuk a nem megfelelő értéket az érték összegekhez

def FillDiscs(): # Ez a függvény feltölti a korongokat véletlen választott csempékkel
	cycleCount: int = 0
	# Választunk véletlen csempéket és a korongokra soroljuk őket
	while (cycleCount < len(discs)):
		tileCount: int = len(discs[cycleCount])
		while (tileCount < TILES_PER_DISC):
			discs[cycleCount].append(GetRandomTile())
			tileCount += 1
		cycleCount += 1
	# TODO: Támogatnia kell a részleges korong feltöltést

def GetChoosableDiscs(): # Összerak egy listát a még választható korongok sorszámaival mint tartalom
	choosable: list = list()
	for index, disc in enumerate(discs, 1):
		if (len(disc) > 0):
			choosable.append(index)
	return choosable

def IsDeskEmpty(): # Ellenőrzi, hogy teljesen kiürült-e az asztal
	deskValues = desk.values()
	for value in deskValues:
		if (value > 0):
			return False
	return True

def OutputDiscs(): # Kiírja a korongok és az asztal jelenlegi csempe tartalmát
	print("-=-= Korongok =-=-")
	for index, disc in enumerate(discs, 1):
		print(str(index) + " |", disc)
	print("Asztal:", desk)

def ExecutePlayerChoice(player: Player, choice: dict): # Ez a függvény veti össze a játékos választását a játékkal és végzi el a műveleteket
	selectedDisc: list = discs[choice.get("disc")] # A játékos által kiválasztott korong
	selectedColor: str = choice.get("color") # A játékos által kiválasztott csempe szín

	if (choice.get("disc") >= 0):
		tmpDisc: list = selectedDisc.copy()
		for tile in tmpDisc:
			if (tile == selectedColor):
				player.tiles[selectedColor] += 1
			else:
				desk[tile] += 1
			selectedDisc.remove(tile)
	else:
		desk[selectedColor] = 0
		player.tiles[selectedColor] += desk.get(selectedColor)
		if (desk["X"] == 1):
			desk["X"] = 0
			player.tiles["X"] = 1

# Ez a függvény intézi a játékosok választási folyamatát. Ha elfogy az összes csempe az asztalról,
# akkor True-t ad vissza
def MakePlayersChoose():
	for player in players:
		choosableDiscs: list = GetChoosableDiscs() # A válaszható korong sorszámát tartalmazó lista (lehet üres)
		if (len(choosableDiscs) > 0 or not IsDeskEmpty()): # Ha van még válaszható korong vagy az asztalon van még csempe
			OutputDiscs() # Kiírja a korongok és az asztal tartalmát
			print("-=-= " + str(player.index+1) + ". játékos köre =-=-")
			playerChoice: dict = player.Round(choosableDiscs)
			ExecutePlayerChoice(player, playerChoice)
		else:
			print("Elfogyott az összes csempe az asztalról. A fal építés fázis következik.")
			return True
	return False
	

def StartGame(): # Ez a függvény elintézi a játék indulásához szükséges alap műveleteket
	while True:
		consoleInput = input("Adja meg a játékosok számát (2-4): ") # Bekérdezzük a játékosok számát a usertől
		try:
			playerCount: int = int(consoleInput) # Megpróbáljuk számmá cast-olni azt az inputot
			if (playerCount >= MINIMUM_PLAYERS and playerCount <= MAXIMUM_PLAYERS): # Ha a játékosszám 1-5 között van
				
				# Feltöltjük a players listánkat a játékos számnak megfelelő Player object-tel
				playerCycleCount: int = 0
				while (playerCycleCount < playerCount):
					players.append(Player(playerCycleCount))
					playerCycleCount += 1

				# Feltöltjük a korongok listát, játékosszámtól függően (2:5, 3:7, 4:9)
				discCycleCount: int = 0
				discPerPlayer: int = playerCount * 2 + 1
				while (discCycleCount < discPerPlayer):
					discs.append(list())
					discCycleCount += 1
				FillDiscs() # TODO: A korong feltöltést ismételni kell minden körben

				# A húzást addig ismétlő ciklus, amíg teljesen ki nem ürül az asztal
				while True:
					emptyDesk: bool = MakePlayersChoose()
					if (emptyDesk): # Ha kiürült az asztal
						break

				break
			else:
				print("2-4 kell legyen a beírt szám!")
		except ValueError:
			print("Számot írjál már be!")

StartGame() # Elindítjuk a játékot