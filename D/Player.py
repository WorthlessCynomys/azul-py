from Constans import COLOR_SELECTION_OPTIONS
from TemplateRow import TemplateRow

class Player:
	index: int # A játékos sorszáma
	tiles: dict # A játékosnál lévő csempék
	templateRows: list # A játékos minta sorai

	def __init__(self, index: int):
		self.index = index
		self.tiles = {"A": 0, "B": 0, "C": 0, "D": 0, "E": 0}

		# Feltöljtük a játékos minta sor listáját 1-5 hosszú minta sorokkal
		temporRows: list = list() # Az ideiglenes tömb amibe pakoljuk a minta sorokat
		templateRowWhile: int = 0 # A while ciklusváltozója
		while (templateRowWhile <= 4):
			temporRows.append(TemplateRow(templateRowWhile+1))
			templateRowWhile += 1
		self.templateRows = temporRows.copy() # Átmásoljuk az idg. tömb tartalmát az objektum tömbjébe

	def Round(self, choosableDiscs: list): # A játékos választási folyamatát végzi el. Bevárja a válaszható korongok listáját
		selectedDisc: int # A kiválaszott korong 
		selectedColor: int # A kiválasztott csempe típus

		while True:
			try:
				selectedDisc = int(input("Válasszon korongot (Asztal - 0): "))
				if (selectedDisc in choosableDiscs or selectedDisc == 0):
					while True:
						selectedColor: str = input("Válasszon színt (A-E): ").upper()
						if (selectedColor in COLOR_SELECTION_OPTIONS):
							return {"disc": selectedDisc-1, "color": selectedColor} # Levonunk egyet a kiválaszott korongból az indexelés miatt
						else:
							print("Nem létező színt adtál meg.")
					break
				else:
					print("Nem jó korong számot adtál meg.")
			except ValueError:
				print("Számot írjál már be!")

	def OutputTiles(self): # Kiírjuk a játékos "kezében" lévő csempéket
		print("Player[" + str(self.index) + "] tiles:", self.tiles)

	def OutputTemplateRows(self): # Kiírjuk a játékos összes minta sorát
		for row in self.templateRows:
			print("Minta sor (" + str(row.rowLength) + ")", row.row)