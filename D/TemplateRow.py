class TemplateRow:
	row: list # A mintasort reprezentáló lista
	rowLength: int # A mintasor hossza

	def __init__(self, rowLength: int):
		self.row = list()
		self.rowLength = rowLength

	def OutputRow(self): # Kiírja a minta sort
		print(str(self.rowLength) + " hosszú mintasor: ", self.row)

	def AddTile(self, tile: str): # Ellenőrzi, hogy fér-e még a sorba, illetve, hogy illik-e a sorba csempe és belerakja
		tileAmount: int = len(self.row) # A mintasorban jelenleg lévő csempék száma
		if (tileAmount > 0):
			if (tileAmount < self.rowLength):
				if (tile == self.row[0]):
					self.row.append(tile)
				else:
					print("Ebbe a mintasorba nem rakhatod be a választott csempét.")
					self.OutputRow()
					return False
			else:
				print("Nem fér több csempe ebbe a mintasorba.")
				self.OutputRow()
				return False
		else:
			self.row.append(tile)

	def GetFirstTile(self): # Visszaadja az első csempét és azt, hogy mennyi megy a kukába
		returnValue: dict = { "tile": self.row[0], "trash": self.rowLength-1 }
		self.row = list()
		return returnValue